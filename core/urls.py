from django.urls import path

from core.views import Home, Data

urlpatterns = [
    path('', Home.as_view(), name='index'),
    path('data/', Data.as_view(), name='data'),
    path('data/<int:id>', Data.as_view(), name='data'),
]
