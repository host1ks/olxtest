from django.db import models


class Post(models.Model):
    name = models.CharField(max_length=255)
    price = models.IntegerField()
    seller_name = models.CharField(max_length=50, blank=True, null=True)


class PostPhoto(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='photo')
    photo = models.URLField()
