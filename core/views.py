import re
from concurrent.futures import ThreadPoolExecutor

import requests
from bs4 import BeautifulSoup
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponse
from django.shortcuts import redirect
from django.views import View
from django.views.generic import TemplateView

from core.models import Post, PostPhoto

headers = {
    'accept': 'text / html, application / xhtml + xml, application / xml;q = 0.9, '
              'image / avif, image / webp, image / apng, * / *;q = 0.8, application '
              '/ signed - exchange;v = b3;q = 0.9',
    'accept - encoding': 'gzip, deflate, br',
    'accept - language': 'en - US, en; q = 0.9, ru;q = 0.8',
    'cache - control': 'no - cache',
}


class Home(LoginRequiredMixin, TemplateView):
    template_name = 'home.html'


class Data(LoginRequiredMixin, View):
    @staticmethod
    def get_data(post, headers):
        post_url = post.find('a').get('href')
        request = requests.get(post_url, headers=headers)
        soup = BeautifulSoup(request.text, features="html.parser")
        name = soup.find('h1')
        price = soup.find('h3')
        seller_name = soup.find('h4')
        photo = soup.find_all('img', {'data-testid': 'swiper-image-lazy'}) + soup.find_all('img', {
            'data-testid': 'swiper-image'})
        post = Post.objects.create(name=name.text,
                                   price=re.findall("\d+", re.sub(r"\s+", "", price.text))[0],
                                   seller_name=seller_name.text)
        for img in photo:
            PostPhoto.objects.create(post=post, photo=img.attrs['data-src'])

    def get(self, request, *args, **kwargs):
        page = 1

        posts_count = request.user.type * 100
        posts = []
        while len(posts) < posts_count:
            a = requests.get(f'https://www.olx.ua/nedvizhimost/?page={page}', headers=headers)
            soup = BeautifulSoup(a.text, features="html.parser")
            posts += soup.find_all('tr', class_='wrap')
            page += 1
        with ThreadPoolExecutor(max_workers=50) as executor:
            for post in posts[:posts_count]:
                executor.submit(self.get_data, post, headers)
        t = {
            1: ['id', 'name', 'price'],
            2: ['id', 'name', 'price', ],
            3: ['id', 'name', 'price', 'seller_name'],
        }
        posts_list = list(Post.objects.all().order_by('-id').values(*t[request.user.type])[:posts_count])
        if request.user.type > 1:
            for post in posts_list:
                post['photo'] = list(PostPhoto.objects.filter(post_id=post['id']).values_list('photo'))
        return JsonResponse(posts_list, safe=False)

    def delete(self, request, *args, **kwargs):
        Post.objects.get(id=kwargs.get('id')).delete()
        return HttpResponse("Deleted")


def view_404(request, exception=None):
    return redirect('index')
