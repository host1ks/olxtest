from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from olxtest import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('users.urls')),
    path('core/', include('core.urls'))
]
handler404 = 'core.views.view_404'
